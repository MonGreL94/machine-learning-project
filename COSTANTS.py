from sklearn.preprocessing import LabelEncoder
import pyaudio

my_labels = [16, 23, 47, 49, 53, 67, 74, 81, 288, 343, 395, 396]
lb = LabelEncoder()
lb.fit_transform(my_labels)
null_vector = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
label_to_class = {16: "Risata", 23: "Pianto bimbo", 47: "Tosse", 49: "Starnuto", 53: "Passi, camminare", 67: "Applauso", 74: "Cane", 81: "Gatto", 288: "Acqua", 343: "Motore", 395: "Sveglia", 396: "Sirena"}
n_classes = 12
input_size = 128
CHUNK = 8192
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
WAVE_OUTPUT_FILENAME = "output.wav"


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
