# Machine Learning Project

This repo contains the code and documentation of the project realized for the Machine Learning & Big Data Analytics course, developed during my Master's Degree course in Computer Engineering.

# Audio Event Recognition

-   The file useful for the classification is "classify.py".

-   The file used for training is "train.py".

-   The file used to extract the dataset from tfrecord is "features_extractor.py".
