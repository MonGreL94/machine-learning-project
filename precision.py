from sklearn.metrics import confusion_matrix
import numpy as np


def precision_recall_fscore(y_true,y_pred, show_probability=False):
    X = confusion_matrix(y_true,y_pred)
    n = len(X)
    TP = {}
    FN = {}
    FP = {}
    for i in range(n):
        FN[i] = 0
        for j in range(n):
            if i == j:
                TP[i] = X[i][j]
            else:
                try:
                    FP[j] = FP[j] + X[i][j]
                except KeyError:
                    FP[j] = X[i][j]
                FN[i] = FN[i] + X[i][j]
    recall = {}
    precision = {}
    f_score = {}
    for i in range(n):
        if TP[i]+FN[i] == 0:
            recall[i] = 0
        else:
            recall[i] = TP[i] / (TP[i] + FN[i])
        if not (TP[i]+FP[i]) == 0:
            precision[i] = TP[i]/(TP[i]+FP[i])
        else:
            precision[i] = 0
        if not (precision[i]+recall[i]) == 0:
            f_score[i] = 2*((precision[i]*recall[i])/(precision[i]+recall[i]))
        else:
            f_score[i] = 0

    if show_probability:
        conf_mat = []
        for i in range(n):
            conf_mat_row = []
            for j in range(n):
                conf_mat_row.append(round(X[i][j]/sum(X[i])*100, 1))
            conf_mat.append(conf_mat_row)
        X = np.array(conf_mat)

    return X, precision, recall, f_score
