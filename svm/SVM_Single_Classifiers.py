from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from parse_data import parse_file
from precision import precision_recall_fscore
from statistics import mean
import numpy as np
import pickle

print("START LOAD TRAINING SET")

training_set = parse_file("../dataSet/bal_train_features.txt")

X = []
y = []

for tf_data in training_set:
    np_vector = np.zeros((128))
    for line_vector in tf_data.arr:
        np_vector = np_vector + line_vector
    np_vector = np_vector / tf_data.n
    X.append(np_vector)
    y.append(tf_data.label)

print("FINISH LOAD TRAINING SET")

print("START LOAD VALIDATION SET")

validation_set = parse_file("../dataSet/eval_features.txt")

Xv = []
yv = []

for tf_data in validation_set:
    np_vector = np.zeros((128))
    for line_vector in tf_data.arr:
        np_vector = np_vector + line_vector
    np_vector = np_vector / tf_data.n
    Xv.append(np_vector)
    yv.append(tf_data.label)

print("FINISH LOAD VALIDATION SET")

X = np.array(X)
y = np.array(y)

Xv = np.array(Xv)
yv = np.array(yv)

with open("SVM_Params.txt", 'r') as my_file:
    text = my_file.read()

text = text.split("SVC con kernel Polinomiale\n")[1]
text = text.split("SVC con kernel RBF\n")
pol_text = text[0]
rbf_text = text[1]
rbf_best_params = rbf_text.split("Parametri migliori: ")[1]
rbf_best_params = rbf_best_params.split("Score su training set: ")[0]
pol_best_params = pol_text.split("Parametri migliori: ")[1]
pol_best_params = pol_best_params.split("Score su training set: ")[0]

C_rbf = float(rbf_best_params.split("{'C': ")[1].split(", 'class_weight': ")[0])
class_weight_rbf = rbf_best_params.split("'class_weight': ")[1].split(", 'decision_function_shape': ")[0]
if class_weight_rbf == "None":
    class_weight_rbf = None
elif class_weight_rbf[0] == "'":
    class_weight_rbf = class_weight_rbf.split("'")[1]
print("class_weight_rbf: ", class_weight_rbf)
decision_function_shape_rbf = rbf_best_params.split("'decision_function_shape': '")[1].split("', 'gamma': ")[0]
gamma_rbf = float(rbf_best_params.split("'gamma': ")[1].split(", 'kernel': ")[0])
tol_rbf = float(rbf_best_params.split("'tol': ")[1].split("}")[0])

C_pol = float(pol_best_params.split("{'C': ")[1].split(", 'class_weight': ")[0])
class_weight_pol = pol_best_params.split("'class_weight': ")[1].split(", 'decision_function_shape': ")[0]
if class_weight_pol == "None":
    class_weight_pol = None
elif class_weight_pol[0] == "'":
    class_weight_pol = class_weight_pol.split("'")[1]
print("class_weight_pol: ", class_weight_pol)
decision_function_shape_pol = pol_best_params.split("'decision_function_shape': '")[1].split("', 'degree': ")[0]
degree_pol = int(pol_best_params.split("'degree': ")[1].split(", 'gamma': ")[0])
gamma_pol = float(pol_best_params.split("'gamma': ")[1].split(", 'kernel': ")[0])
tol_pol = float(pol_best_params.split("'tol': ")[1].split("}")[0])

rbf_params = {'C': [C_rbf], 'class_weight': [class_weight_rbf], 'decision_function_shape': [decision_function_shape_rbf], 'gamma': [gamma_rbf], 'tol': [tol_rbf], 'kernel': ['rbf']}
pol_params = {'C': [C_pol], 'class_weight': [class_weight_pol], 'decision_function_shape': [decision_function_shape_pol], 'degree': [degree_pol], 'gamma': [gamma_pol], 'tol': [tol_pol], 'kernel': ['poly']}

svm = SVC()
rbf_svm = GridSearchCV(svm, rbf_params, n_jobs=-1, cv=5, verbose=False)
pol_svm = GridSearchCV(svm, pol_params, n_jobs=-1, cv=5, verbose=False)

print("START FITTING SVM_RBF CLASSIFIER")
rbf_model = rbf_svm.fit(X, y)
rbf_svm_train_score = rbf_svm.score(X, y)
print("Score su training set SVM_RBF:", rbf_svm_train_score)
rbf_svm_test_score = rbf_svm.score(Xv, yv)
print("Score sul test set SVM_RBF:", rbf_svm_test_score)

print("START FITTING SVM_POL CLASSIFIER")
pol_model = pol_svm.fit(X, y)
pol_svm_train_score = pol_svm.score(X, y)
print("Score su training set SVM_POL:", pol_svm_train_score)
pol_svm_test_score = pol_svm.score(Xv, yv)
print("Score sul test set SVM_POL:", pol_svm_test_score)

predicted_labels_rbf = rbf_svm.predict(Xv)
predicted_labels_pol = pol_svm.predict(Xv)

confusion_matrix_rbf, precision_rbf, recall_rbf, f_score_rbf = precision_recall_fscore(yv.tolist(), predicted_labels_rbf.tolist(), True)
confusion_matrix_pol, precision_pol, recall_pol, f_score_pol = precision_recall_fscore(yv.tolist(), predicted_labels_pol.tolist(), True)

print("CONFUSION MATRIX SVM_RBF")
print(confusion_matrix_rbf)
print("PRECISION - RECALL - F-SCORE SVM_RBF")
print(precision_rbf)
print(recall_rbf)
print(f_score_rbf)
print(mean(f_score_rbf.values()))

print("CONFUSION MATRIX SVM_POL")
print(confusion_matrix_pol)
print("PRECISION - RECALL - F-SCORE SVM_POL")
print(precision_pol)
print(recall_pol)
print(f_score_pol)
print(mean(f_score_pol.values()))

print("SALVATAGGIO MODELLI")
with open("SVM_RBF_Model.pkl", 'wb') as rbf_file:
    pickle.dump(rbf_svm, rbf_file)
with open("SVM_POL_Model.pkl", 'wb') as pol_file:
    pickle.dump(pol_svm, pol_file)
