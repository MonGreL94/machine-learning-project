from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from parse_data import parse_file
from precision import precision_recall_fscore
from statistics import mean
import numpy as np
import pickle

print("START LOAD TRAINING SET")

training_set = parse_file("../dataSet/bal_train_features.txt")

X = []
y = []

for tf_data in training_set:
    np_vector = np.zeros((128))
    for line_vector in tf_data.arr:
        np_vector = np_vector + line_vector
    np_vector = np_vector / tf_data.n
    X.append(np_vector)
    y.append(tf_data.label)

print("FINISH LOAD TRAINING SET")

print("START LOAD VALIDATION SET")

validation_set = parse_file("../dataSet/eval_features.txt")

Xv = []
yv = []

for tf_data in validation_set:
    np_vector = np.zeros((128))
    for line_vector in tf_data.arr:
        np_vector = np_vector + line_vector
    np_vector = np_vector / tf_data.n
    Xv.append(np_vector)
    yv.append(tf_data.label)

print("FINISH LOAD VALIDATION SET")

X = np.array(X)
y = np.array(y)

Xv = np.array(Xv)
yv = np.array(yv)

svm = SVC()

grid_rbf = GridSearchCV(svm, {"C": [1.775, 1.8, 1.825], "kernel": ["rbf"], "gamma": [2.165e-06, 2.175e-06, 2.185e-06], "decision_function_shape": ["ovo", "ovr", "crammer_singer"], "class_weight": [None, "balanced"], "tol": [1e-03, 1e-06, 1e-09]}, cv=5, verbose=False, n_jobs=-1)
grid_pol = GridSearchCV(svm, {"C": [0.0015, 0.002, 0.0025], "kernel": ["poly"], "degree": [4, 5, 6], "gamma": [2.05e-06, 2.1e-06, 2.15e-06], "decision_function_shape": ["ovo", "ovr", "crammer_singer"], "class_weight": [None, "balanced"], "tol": [1e-03, 1e-06, 1e-09]}, cv=5, verbose=False, n_jobs=-1)

print("START FITTING SVM_RBF CLASSIFIERS")
grid_rbf.fit(X, y)
print("Parametri migliori per SVM_RBF:", grid_rbf.best_params_)
grid_rbf_train_score = grid_rbf.score(X, y)
print("Score su training set SVM_RBF:", grid_rbf_train_score)
grid_rbf_test_score = grid_rbf.score(Xv, yv)
print("Score sul test set SVM_RBF:", grid_rbf_test_score)

print("START FITTING SVM_POL CLASSIFIERS")
grid_pol.fit(X, y)
print("Parametri migliori per SVM_POL:", grid_pol.best_params_)
grid_pol_train_score = grid_pol.score(X, y)
print("Score su training set SVM_POL:", grid_pol_train_score)
grid_pol_test_score = grid_pol.score(Xv, yv)
print("Score sul test set SVM_POL:", grid_pol_test_score)

predicted_labels_rbf = grid_rbf.predict(Xv)
predicted_labels_pol = grid_pol.predict(Xv)

confusion_matrix_rbf, precision_rbf, recall_rbf, fscore_rbf = precision_recall_fscore(yv.tolist(), predicted_labels_rbf.tolist(), True)
confusion_matrix_pol, precision_pol, recall_pol, fscore_pol = precision_recall_fscore(yv.tolist(), predicted_labels_pol.tolist(), True)

print("CONFUSION MATRIX SVM_RBF")
print(confusion_matrix_rbf)
print("PRECISION - RECALL - F-SCORE SVM_RBF")
print(precision_rbf)
print(recall_rbf)
print(fscore_rbf)
print(mean(fscore_rbf.values()))

print("CONFUSION MATRIX SVM_POL")
print(confusion_matrix_pol)
print("PRECISION - RECALL - F-SCORE SVM_POL")
print(precision_pol)
print(recall_pol)
print(fscore_pol)
print(mean(fscore_pol.values()))

print("SALVATAGGIO MODELLI")
with open("SVM_GRID_RBF_Model.pkl", 'wb') as rbf_file:
    pickle.dump(grid_rbf, rbf_file)
with open("SVM_GRID_POL_Model.pkl", 'wb') as pol_file:
    pickle.dump(grid_pol, pol_file)

print("SALVATAGGIO PARAMETRI MIGLIORI")
with open("SVM_Params.txt", 'w') as my_file:
    my_file.write("SVC con kernel Polinomiale\n")
    my_file.write("Parametri migliori: " + str(grid_pol.best_params_) + "\n")
    my_file.write("Score su training set: " + str(grid_pol_train_score) + "\n")
    my_file.write("Score su test set: " + str(grid_pol_test_score) + "\n")
    my_file.write("F-Score medio: " + str(mean(fscore_pol.values())) + "\n\n")

    my_file.write("SVC con kernel RBF\n")
    my_file.write("Parametri migliori: " + str(grid_rbf.best_params_) + "\n")
    my_file.write("Score su training set: " + str(grid_rbf_train_score) + "\n")
    my_file.write("Score su test set: " + str(grid_rbf_test_score) + "\n")
    my_file.write("F-Score medio: " + str(mean(fscore_rbf.values())))
