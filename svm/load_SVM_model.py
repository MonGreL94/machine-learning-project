from precision import precision_recall_fscore
from parse_data import parse_file
import numpy as np
import pickle
from statistics import mean

print("START LOAD VALIDATION SET")

test_set = parse_file("../test_set_features/eval_features.txt")

Xv = []
yv = []

for tf_data in test_set:
    np_vector = np.zeros((128))
    for line_vector in tf_data.arr:
        np_vector = np_vector + line_vector
    np_vector = np_vector / tf_data.n
    Xv.append(np_vector)
    yv.append(tf_data.label)

Xv = np.array(Xv)
yv = np.array(yv)

print("FINISH LOAD VALIDATION SET")

print("START LOAD MODELS")

with open("SVM_RBF_Model.pkl", 'rb') as rbf_file:
    rbf_model = pickle.load(rbf_file)

with open("SVM_POL_Model.pkl", 'rb') as pol_file:
    pol_model = pickle.load(pol_file)

print("FINISH LOAD MODELS")

predicted_labels_rbf = rbf_model.predict(Xv)
predicted_labels_pol = pol_model.predict(Xv)

confusion_matrix_rbf, precision_rbf, recall_rbf, f_score_rbf = precision_recall_fscore(yv.tolist(), predicted_labels_rbf.tolist())
confusion_matrix_pol, precision_pol, recall_pol, f_score_pol = precision_recall_fscore(yv.tolist(), predicted_labels_pol.tolist())

score_rbf = 0
for i in range(Xv.__len__()):
    if predicted_labels_rbf[i] == yv[i]:
        score_rbf = score_rbf + 1
accuracy_rbf = score_rbf/Xv.__len__()

score_pol = 0
for i in range(Xv.__len__()):
    if predicted_labels_pol[i] == yv[i]:
        score_pol = score_pol + 1
accuracy_pol = score_pol / Xv.__len__()

print("CONFUSION MATRIX SVM RBF")
print(confusion_matrix_rbf)
print("PRECISION - RECALL - F-SCORE SVM_RBF")
print(mean(f_score_rbf.values()))
print(accuracy_rbf)

print("CONFUSION MATRIX SVM POL")
print(confusion_matrix_pol)
print("PRECISION - RECALL - F-SCORE SVM_POL")
print(mean(f_score_pol.values()))
print(accuracy_pol)
