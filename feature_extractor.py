import tensorflow as tf
import os
from COSTANTS import my_labels

my_dirs = ["bal_train", "eval", "unbal_train"]
example = tf.train.SequenceExample()


def extract_features(my_dir):
    count = 0
    filename = "test_set_features/" + my_dir + "_features.txt"
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as my_file:
        for file in os.listdir("test_set/" + my_dir):
            for string_record in tf.python_io.tf_record_iterator(path="test_set/" + my_dir + "/" + file):
                example.ParseFromString(string_record)
                pattern_labels = set()
                for label in example.context.feature["labels"].int64_list.value:
                    pattern_labels.add(label)
                set_labels = set(my_labels)
                labels_intersection = set_labels.intersection(pattern_labels)
                if labels_intersection.__len__() == 1:
                    my_all_features = []
                    for pattern_feature_vector in example.feature_lists.feature_list["audio_embedding"].feature:
                        my_single_feature = []
                        for byte in pattern_feature_vector.bytes_list.value[0]:
                            my_single_feature.append(byte)
                        my_all_features.append(my_single_feature)

                    my_file.write("{" + str(my_all_features) + "; ")
                    my_file.write(str(my_all_features.__len__()) + ", ")
                    my_file.write(str(list(labels_intersection)[0]) + "}\n")
                    count = count + 1
    return count


count1 = extract_features(my_dirs[0])
count2 = extract_features(my_dirs[1])
count3 = extract_features(my_dirs[2])
print(count1)
print(count2)
print(count3)
print(count1 + count2 + count3)
