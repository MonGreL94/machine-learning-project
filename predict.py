import numpy as np
from COSTANTS import lb


def predict(model, input, input_shape, categorical=True):
    input = input.reshape(input_shape)
    output = model.predict_on_batch(input)
    # print(output)
    if categorical:
        output = np.argmax(output)
        # print(output)
    output = lb.inverse_transform([output])
    return output[0]
