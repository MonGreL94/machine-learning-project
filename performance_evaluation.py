from precision import precision_recall_fscore
from statistics import mean
from keras.models import load_model
from parse_data import parse_file
from predict import predict
import numpy as np

model = load_model("model/CNN_Model.h5")

test_set = parse_file("test_set_features/eval_features.txt")

expected = []
predicted = []

score = 0

for pattern in test_set:
    expected.append(pattern.label)
    predicted.append(predict(model, np.array(pattern.arr), (1, 128*pattern.n, 1)))
    if pattern.label == predict(model, np.array(pattern.arr), (1, 128*pattern.n, 1)):
        score = score + 1

accuracy = score/test_set.__len__()

confusion_matrix, precision, recall, f_score = precision_recall_fscore(expected, predicted)

print("Confusion matrix")
print()
print(confusion_matrix)
print()
print("F-Score medio su tutte le classi")
print()
print(mean(f_score.values()))
print()
print("Percentuale di patterns classificati correttamente")
print()
print(accuracy)
