import ast


class TFData:
    def __init__(self, arr, n, label):
        self.arr = arr
        self.n = n
        self.label = label


def parse_record(stringa):
    first = stringa.split('{')
    second = first[1].split(';')
    first = second[0]
    second = second[1].split(',')
    arr = ast.literal_eval(first)
    n = int(second[0])
    second = second[1].split('}')
    label = int(second[0])
    return arr, n, label


def parse_data(text):
    for s in text.split('\n'):
        if s.__contains__(";"):
            arr, n, label = parse_record(s)
            yield arr, n, label


def parse_file(filename):
    with open(filename, 'r') as my_file:
        stringa = my_file.read()
        data_vector = []
        for arr, n, label in parse_data(stringa):
            single_data = TFData(arr, n, label)
            data_vector.append(single_data)

    return data_vector


def merge_data():
    filename = 'test_set_features/bal_train_features.txt'
    data_vector = parse_file(filename)
    filename = 'test_set_features/eval_features.txt'
    data_vector2 = parse_file(filename)
    filename = 'test_set_features/unbal_train_features.txt'
    data_vector3 = parse_file(filename)
    data_vector.extend(data_vector2)
    data_vector.extend(data_vector3)
    return data_vector
