import keras
import numpy as np
from tqdm import tqdm
from COSTANTS import lb, n_classes
from model import my_model
from parse_data import parse_file
from predict import predict
from precision import precision_recall_fscore
import os


def fit(model, n_epochs, n_batches, dataset, validationset, model_type='CNN'):
    max = 0
    for i in range(n_epochs):
        # Training
        print("Training...")
        print("Epoca: {}".format(i + 1))
        epLoss, epAcc = train_model(model, dataset, n_batches, model_type)
        print("Train_loss: {}; Train_acc: {}%".format(epLoss, epAcc*100))

        # Validation
        expected = []
        predicted = []
        epLoss = 0
        epAcc = 0
        n = len(validationset)
        print("Validating...")
        for tfrecord in tqdm(validationset):
            input = np.array(tfrecord.arr)
            # input della rete per il validation
            shape_input = input_shape(input, model_type)
            input = input.reshape(shape_input)
            a = tfrecord.label
            b = predict(model, input, shape_input)
            expected.append(a)
            predicted.append(b)
            # calcolo prestazioni validation
            x = np.array(input)
            y = lb.transform([a])
            y = keras.utils.to_categorical(y, num_classes=n_classes)
            metrics = model.test_on_batch(x, y, sample_weight=None)
            currentLoss, currentAcc = metrics[0], metrics[1]
            epLoss = epLoss + currentLoss
            epAcc = epAcc + currentAcc

        print("Val_loss: {}; Val_acc: {}%".format(epLoss/n, epAcc/n*100))
        X, precision, recall, f_score = precision_recall_fscore(expected, predicted)
        sum = 0
        for i in range(12):
            if f_score is not None:
                sum = sum + f_score[i]
                # print("Class: {}, Precision: {}, Recall: {}, F-Score: {}".format(i,precision[i],recall[i],f_score[i]))
        score = sum / 12 * 100
        print("Punteggio: %.2f%%" % (score))
        if max < score:
            max = score
            file_name = "model/CNN_Model_{}.h5".format(score)
            os.makedirs(os.path.dirname(file_name), exist_ok=True)
            model.save(file_name)
    return model


def train_model(model, dataset, n_batch, model_type):
    epLoss = 0
    epAcc = 0
    n = len(dataset)
    count = 0
    for tfrecord in tqdm(dataset):
        batch = np.array(tfrecord.arr)
        # input della rete per il training
        batch = batch.reshape(input_shape(batch, model_type))
        label = lb.transform([tfrecord.label])
        label = keras.utils.to_categorical(label, num_classes=n_classes)
        c = model.train_on_batch(batch, label)
        currentLoss, currentAcc = c[0], c[1]

        epLoss = epLoss + currentLoss
        epAcc = epAcc + currentAcc

        count = (count + 1) % n_batch
        if count == 0:
            model.reset_states()

    return epLoss / n, epAcc / n


def input_shape(input, type):
    if type == 'CNN':
        return input_shape_CNN(input)
    if type == 'RNN':
        return input_shape_RNN(input)


def input_shape_CNN(input):
    return 1, input.shape[0] * input.shape[1], 1


def input_shape_RNN(input):
    return 1, input.shape[0], input.shape[1]


model = my_model()
model.summary()
opt = keras.optimizers.sgd(lr=0.01)
model.compile(optimizer=opt, loss='mse', metrics=['accuracy'])

n_epochs = 50
n_batches = 1

"""TRAINING SU BALL+UNBALL / VALIDATION SU EVAL"""
dataset = parse_file('dataSet/bal_train_features.txt')
dataset1 = parse_file('dataSet/unbal_train_features.txt')
dataset.extend(dataset1)
validationset = parse_file('dataSet/eval_features.txt')

model = fit(model, n_epochs, n_batches, dataset, validationset, "CNN")
