from pynput import keyboard
import time
import pyaudio
import wave
import sched
import sys
import tensorflow as tf
from audioset import vggish_input, vggish_params, vggish_postprocess, vggish_slim
from keras.models import load_model
from COSTANTS import label_to_class, lb, bcolors, CHUNK, FORMAT, CHANNELS, RATE, WAVE_OUTPUT_FILENAME

p = pyaudio.PyAudio()
frames = []


def load_my_model():
    model = load_model("model/CNN_Model.h5")
    return model


def predict(model, pattern):
    result = model.predict(pattern)
    result = result.argmax()
    result = lb.inverse_transform([result])
    return result


def callback(in_data, frame_count, time_info, status):
    frames.append(in_data)
    sec = int(frames.__len__() * CHUNK / RATE)
    if frames.__len__() * CHUNK % 40960 == 0:
        print(sec + 1, "Seconds")
    return in_data, pyaudio.paContinue


class MyListener(keyboard.Listener):
    def __init__(self):
        super(MyListener, self).__init__(self.on_press, self.on_release)
        self.key_pressed = None
        self.wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
        self.wf.setnchannels(CHANNELS)
        self.wf.setsampwidth(p.get_sample_size(FORMAT))
        self.wf.setframerate(RATE)

    def on_press(self, key):
        if key.char == 'r':
            self.key_pressed = True
        return True

    def on_release(self, key):
        if key.char == 'r':
            self.key_pressed = False
        return True


listener = MyListener()
listener.start()
started = False
stream = None


def recorder():
    global started, p, stream, frames

    if listener.key_pressed and not started:
        # Start the recording
        try:
            stream = p.open(format=FORMAT,
                            channels=CHANNELS,
                            rate=RATE,
                            input=True,
                            frames_per_buffer=CHUNK,
                            stream_callback=callback)
            # print("Stream active:", stream.is_active())
            started = True
            print()
            print(bcolors.OKGREEN, "START STREAM", bcolors.ENDC)
        except:
            raise

    elif not listener.key_pressed and started:
        print(bcolors.FAIL, "STOP RECORDING", bcolors.ENDC)
        stream.stop_stream()
        stream.close()
        p.terminate()
        listener.wf.writeframes(b''.join(frames))
        listener.wf.close()
        if frames.__len__() >= 5:
            postprocessed_batch = embedding_wav("output.wav")
            print('Postprocessed VGGish embedding: ', postprocessed_batch.shape)
            model = load_my_model()
            result = predict(model,
                             postprocessed_batch.reshape(1, postprocessed_batch.shape[0] * postprocessed_batch.shape[1],
                                                         1))
            print("Label: ", result)
            print("Classe: ", label_to_class.get(result[0]))
        else:
            print("Registra per più di un secondo per favore :)")
        sys.exit()
    # Reschedule the recorder function in 100 ms.
    task.enter(0.1, 1, recorder, ())


def embedding_wav(filename):
    checkpoint_path = 'audioset/vggish_model.ckpt'
    pca_params_path = 'audioset/vggish_pca_params.npz'

    # Take wav file extract data and returns:
    # 3-D np.array of shape [num_examples, num_frames, num_bands] which represents
    # a sequence of examples, each of which contains a patch of log mel
    # spectrogram, covering num_frames frames of audio and num_bands mel frequency
    # bands, where the frame length is vggish_params.STFT_HOP_LENGTH_SECONDS.

    input_batch = vggish_input.wavfile_to_examples(filename)
    print(input_batch.shape)

    # Pre-classifier neural network
    with tf.Graph().as_default(), tf.Session() as sess:
        vggish_slim.define_vggish_slim()
        vggish_slim.load_vggish_slim_checkpoint(sess, checkpoint_path)
        features_tensor = sess.graph.get_tensor_by_name(
            vggish_params.INPUT_TENSOR_NAME)
        embedding_tensor = sess.graph.get_tensor_by_name(
            vggish_params.OUTPUT_TENSOR_NAME)
        [embedding_batch] = sess.run([embedding_tensor],
                                     feed_dict={features_tensor: input_batch})

    # Postprocessing
    pproc = vggish_postprocess.Postprocessor(pca_params_path)
    postprocessed_batch = pproc.postprocess(embedding_batch)

    return postprocessed_batch


"""print("Begin Recording: \t Press and hold the 'R' key")
print("End Recording: \t \t Release the 'R' key")
task = sched.scheduler(time.time, time.sleep)
task.enter(0.1, 1, recorder, ())
task.run()"""
