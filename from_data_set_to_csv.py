from parse_data import parse_file
import os


def from_data_set_to_csv(txt_features_file, saving_folder):
    i = 0
    data_set = parse_file("BalancedDataSet/" + txt_features_file)
    for tf_data in data_set:
        text_to_write = ""
        for j in range(tf_data.n):
            for cnt in range(tf_data.arr[j].__len__()):
                if cnt == tf_data.arr[j].__len__() - 1:
                    if j == tf_data.n - 1:
                        text_to_write = text_to_write + str(tf_data.arr[j][cnt])
                    else:
                        text_to_write = text_to_write + str(tf_data.arr[j][cnt]) + "\n"
                else:
                    text_to_write = text_to_write + str(tf_data.arr[j][cnt]) + ","

        filename = "csv_patterns/" + saving_folder + "/" + saving_folder + "_pattern_" + str(i) + "_" + str(tf_data.label) + ".csv"
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "w") as pattern_file:
            pattern_file.write(text_to_write)
        i = i + 1


#from_data_set_to_csv("bal_train_features.txt", "bal_train")
#from_data_set_to_csv("unbal_train_features.txt", "unbal_train")
from_data_set_to_csv("training_set.txt", "balanced_test_set")
