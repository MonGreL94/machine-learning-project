from parse_data import merge_data, parse_file
import random
import os
from COSTANTS import lb


def return_balanced_subset(patterns, class_size_in_subset):
    balanced_subset = []
    for cls in patterns:
        for _ in range(class_size_in_subset):
            p = random.randint(0, patterns[cls].__len__() - 1)
            tf_pattern = patterns[cls][p]
            balanced_subset.append(tf_pattern)
            patterns[cls].remove(tf_pattern)
    return balanced_subset, patterns


def return_training_set(patterns, dim_classes_bal, dim_small_class, perc, dim_small_class_in_bal):
    training_set = []
    for cls in patterns:
        for _ in range(int(dim_classes_bal[cls]*perc*dim_small_class/dim_small_class_in_bal)):
            p = random.randint(0, patterns[cls].__len__() - 1)
            tf_pattern = patterns[cls][p]
            training_set.append(tf_pattern)
            patterns[cls].remove(tf_pattern)
    return training_set, patterns


def write_set_in_file(file_name, the_set):
    filename = "BalancedDataSet/" + file_name
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as my_file:
        for tf_pattern in the_set:
            my_file.write("{" + str(tf_pattern.arr) + "; " + str(tf_pattern.n) + ", " + str(tf_pattern.label) + "}\n")


def dim_bal_training():
    bal_data = parse_file('test_set_features/bal_train_features.txt')
    dim_classes_bal = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    for tf_data in bal_data:
        i = lb.transform([tf_data.label])[0]
        dim_classes_bal[i] = dim_classes_bal[i] + 1

    return dim_classes_bal


def generate_sets(want_also_test_set=False):
    all_data = merge_data()
    all_patterns = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: [], 11: []}

    for tf_data in all_data:
        i = lb.transform([tf_data.label])[0]
        all_patterns[i].append(tf_data)

    small_class = 0
    dim_small_class = all_patterns[0].__len__()
    for i in all_patterns:
        if all_patterns[i].__len__() < dim_small_class:
            dim_small_class = all_patterns[i].__len__()
            small_class = i

    print("small class --->", small_class, "- dim_small_class --->", dim_small_class)

    dim_classes_bal = dim_bal_training()
    print("Numero di pattern per ogni classe nel training set", dim_classes_bal)
    dim_small_class_in_bal = dim_classes_bal[small_class]
    print("Numero di pattern della classe più piccola nel training set", dim_small_class_in_bal)

    perc = 0.98
    training_set, all_patterns = return_training_set(all_patterns, dim_classes_bal, dim_small_class, perc, dim_small_class_in_bal)
    random.shuffle(training_set)

    new_cnt = 0
    for pattern in training_set:
        if lb.transform([pattern.label])[0] == small_class:
            new_cnt = new_cnt + 1

    print("Numero di pattern della classe più piccola nel nuovo training set", new_cnt)

    validation_set = []
    test_set = []

    for cls in all_patterns:
        while all_patterns[cls].__len__() != 0:
            p = random.randint(0, all_patterns[cls].__len__() - 1)
            tf_pattern = all_patterns[cls][p]
            validation_set.append(tf_pattern)
            all_patterns[cls].remove(tf_pattern)
            if want_also_test_set and (all_patterns[cls].__len__() != 0):
                p = random.randint(0, all_patterns[cls].__len__() - 1)
                tf_pattern = all_patterns[cls][p]
                test_set.append(tf_pattern)
                all_patterns[cls].remove(tf_pattern)

    random.shuffle(validation_set)
    random.shuffle(test_set)

    print(all_data.__len__())
    print(training_set.__len__() + validation_set.__len__() + test_set.__len__())
    print((training_set.__len__() + validation_set.__len__() + test_set.__len__()) == all_data.__len__())
    print(training_set.__len__(), validation_set.__len__(), test_set.__len__())
    print(sum([all_patterns[cls].__len__() for cls in all_patterns]))
    print(sum([all_patterns[cls].__len__() for cls in all_patterns]) == 0)

    write_set_in_file("training_set.txt", training_set)
    write_set_in_file("validation_set.txt", validation_set)
    if want_also_test_set:
        write_set_in_file("test_set.txt", test_set)


generate_sets()
