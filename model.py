from keras.layers import Conv1D, BatchNormalization, Activation, MaxPooling1D, Dropout, GlobalAveragePooling1D
from keras.models import Sequential


def my_model():
    # creazione modello
    model = Sequential()
    # L1
    model.add(Conv1D(filters=16, kernel_size=9, input_shape=(None, 1), padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Conv1D(filters=16, kernel_size=9, padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(MaxPooling1D())
    model.add(Dropout(0.2))
    # L2
    model.add(Conv1D(filters=32, kernel_size=9, padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Conv1D(filters=32, kernel_size=9, padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(MaxPooling1D())
    model.add(Dropout(0.2))
    # L3
    model.add(Conv1D(filters=64, kernel_size=9, padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Conv1D(filters=64, kernel_size=9, padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(MaxPooling1D())
    model.add(Dropout(0.2))
    # L4
    model.add(Conv1D(filters=128, kernel_size=9, padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Conv1D(filters=128, kernel_size=9, padding='same'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(MaxPooling1D())
    model.add(Dropout(0.2))

    # L8
    model.add(Conv1D(filters=12, kernel_size=1))
    model.add(Activation('sigmoid'))

    # P
    model.add(GlobalAveragePooling1D())
    return model


model = my_model()
