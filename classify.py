import os
import sys
import numpy as np
from keras.models import load_model
from predict import predict
print("STO CLASSIFICANDO")
if len(sys.argv)-1 != 2:
    raise TypeError("classify.py takes 2 positional arguments but " + str(len(sys.argv)-1) + " were given")
else:
    model = load_model("model/CNN_Model.h5")
    with open(sys.argv[2], 'w') as csv_out:
        for file in os.listdir(sys.argv[1]):
            pattern_to_test = []
            with open(sys.argv[1] + '/' + file, 'r') as pattern:
                pattern_text = pattern.read()
            features_vectors = pattern_text.split("\n")
            if features_vectors[-1] == "":
                features_vectors = features_vectors[:-1]
            for vector in features_vectors:
                if "," in vector:
                    features_vector = vector.split(",")
                elif " " in vector:
                    features_vector = vector.split(" ")
                else:
                    print("Formato csv non valido")
                    sys.exit()
                int_features_vector = []
                for feature in features_vector:
                    int_features_vector.append(int(feature))
                pattern_to_test.append(int_features_vector)

            pattern_to_test = np.array(pattern_to_test)
            result = predict(model, pattern_to_test, (1, len(pattern_to_test)*128, 1))
            csv_out.write(file + " " + str(result) + "\n")
